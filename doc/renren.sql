/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : renren

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-12-31 16:06:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) NOT NULL,
  `company_parent_id` bigint(20) DEFAULT NULL,
  `icon` varchar(20) NOT NULL,
  `c_time` date DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('1', '2222', '0', 'icon', '2016-12-23');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_3', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_3', 'DEFAULT', null, 'com.df.renren.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B455973720026636F6D2E64662E72656E72656E2E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200084C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C000A6D6574686F644E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740009746573745461736B317372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001591694A4247874000E302F3130202A202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000374000F746573745461736B4D6574686F643174000672656E72656E740008E6AF8F3130E7A792737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RenrenScheduler', 'PC2016033015581483171170711', '1483171264476', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_3', 'DEFAULT', 'TASK_3', 'DEFAULT', null, '1482142560000', '-1', '5', 'PAUSED', 'CRON', '1482142557000', '0', null, '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B455973720026636F6D2E64662E72656E72656E2E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200084C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C000A6D6574686F644E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001591694A3487874000E302F3130202A202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B020000787000000000000000037400047465737474000672656E72656E740008E6AF8F3130E7A792737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000017800);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('3', 'testTask', 'test', 'renren', '0/10 * * * * ?', '1', '每10秒', '2016-12-19 18:15:57');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=692 DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES ('280', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '6', '2016-12-19 18:18:18');
INSERT INTO `schedule_job_log` VALUES ('281', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '6', '2016-12-19 18:18:21');
INSERT INTO `schedule_job_log` VALUES ('282', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:18:30');
INSERT INTO `schedule_job_log` VALUES ('283', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '5', '2016-12-19 18:18:40');
INSERT INTO `schedule_job_log` VALUES ('284', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:18:50');
INSERT INTO `schedule_job_log` VALUES ('285', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:19:00');
INSERT INTO `schedule_job_log` VALUES ('286', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:19:10');
INSERT INTO `schedule_job_log` VALUES ('287', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:19:21');
INSERT INTO `schedule_job_log` VALUES ('288', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:19:30');
INSERT INTO `schedule_job_log` VALUES ('289', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '9', '2016-12-19 18:19:40');
INSERT INTO `schedule_job_log` VALUES ('290', '3', 'testTask1', 'testTaskMethod1', 'renren', '1', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'testTask1\' is defined', '2', '2016-12-19 18:19:50');
INSERT INTO `schedule_job_log` VALUES ('291', '3', 'testTask', 'test', 'renren', '0', null, '1032', '2016-12-19 18:20:06');
INSERT INTO `schedule_job_log` VALUES ('292', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-19 18:20:10');
INSERT INTO `schedule_job_log` VALUES ('293', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-19 18:20:20');
INSERT INTO `schedule_job_log` VALUES ('294', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-19 18:20:30');
INSERT INTO `schedule_job_log` VALUES ('295', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-19 18:20:40');
INSERT INTO `schedule_job_log` VALUES ('296', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-19 18:20:50');
INSERT INTO `schedule_job_log` VALUES ('297', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-19 18:21:00');
INSERT INTO `schedule_job_log` VALUES ('298', '3', 'testTask', 'test', 'renren', '0', null, '1040', '2016-12-20 13:43:42');
INSERT INTO `schedule_job_log` VALUES ('299', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 13:43:50');
INSERT INTO `schedule_job_log` VALUES ('300', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:44:00');
INSERT INTO `schedule_job_log` VALUES ('301', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:44:10');
INSERT INTO `schedule_job_log` VALUES ('302', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 13:44:20');
INSERT INTO `schedule_job_log` VALUES ('303', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:44:30');
INSERT INTO `schedule_job_log` VALUES ('304', '3', 'testTask', 'test', 'renren', '0', null, '1033', '2016-12-20 13:44:40');
INSERT INTO `schedule_job_log` VALUES ('305', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:44:50');
INSERT INTO `schedule_job_log` VALUES ('306', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:45:01');
INSERT INTO `schedule_job_log` VALUES ('307', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:45:10');
INSERT INTO `schedule_job_log` VALUES ('308', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 13:45:20');
INSERT INTO `schedule_job_log` VALUES ('309', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 13:45:30');
INSERT INTO `schedule_job_log` VALUES ('310', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 13:45:40');
INSERT INTO `schedule_job_log` VALUES ('311', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 13:45:50');
INSERT INTO `schedule_job_log` VALUES ('312', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:46:00');
INSERT INTO `schedule_job_log` VALUES ('313', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:46:10');
INSERT INTO `schedule_job_log` VALUES ('314', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:46:20');
INSERT INTO `schedule_job_log` VALUES ('315', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:46:30');
INSERT INTO `schedule_job_log` VALUES ('316', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:46:40');
INSERT INTO `schedule_job_log` VALUES ('317', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:46:50');
INSERT INTO `schedule_job_log` VALUES ('318', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 13:47:00');
INSERT INTO `schedule_job_log` VALUES ('319', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:47:10');
INSERT INTO `schedule_job_log` VALUES ('320', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:47:20');
INSERT INTO `schedule_job_log` VALUES ('321', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:47:30');
INSERT INTO `schedule_job_log` VALUES ('322', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:47:40');
INSERT INTO `schedule_job_log` VALUES ('323', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 13:47:50');
INSERT INTO `schedule_job_log` VALUES ('324', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:48:00');
INSERT INTO `schedule_job_log` VALUES ('325', '3', 'testTask', 'test', 'renren', '0', null, '1020', '2016-12-20 13:48:10');
INSERT INTO `schedule_job_log` VALUES ('326', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 13:48:20');
INSERT INTO `schedule_job_log` VALUES ('327', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:48:30');
INSERT INTO `schedule_job_log` VALUES ('328', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:48:40');
INSERT INTO `schedule_job_log` VALUES ('329', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:48:50');
INSERT INTO `schedule_job_log` VALUES ('330', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 13:49:00');
INSERT INTO `schedule_job_log` VALUES ('331', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 13:49:10');
INSERT INTO `schedule_job_log` VALUES ('332', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 13:49:20');
INSERT INTO `schedule_job_log` VALUES ('333', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:49:31');
INSERT INTO `schedule_job_log` VALUES ('334', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:49:40');
INSERT INTO `schedule_job_log` VALUES ('335', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:49:50');
INSERT INTO `schedule_job_log` VALUES ('336', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:50:00');
INSERT INTO `schedule_job_log` VALUES ('337', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:50:10');
INSERT INTO `schedule_job_log` VALUES ('338', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:50:20');
INSERT INTO `schedule_job_log` VALUES ('339', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 13:50:30');
INSERT INTO `schedule_job_log` VALUES ('340', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:50:40');
INSERT INTO `schedule_job_log` VALUES ('341', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:50:50');
INSERT INTO `schedule_job_log` VALUES ('342', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:51:00');
INSERT INTO `schedule_job_log` VALUES ('343', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:51:10');
INSERT INTO `schedule_job_log` VALUES ('344', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:51:20');
INSERT INTO `schedule_job_log` VALUES ('345', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:51:30');
INSERT INTO `schedule_job_log` VALUES ('346', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:51:40');
INSERT INTO `schedule_job_log` VALUES ('347', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 13:51:50');
INSERT INTO `schedule_job_log` VALUES ('348', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:52:00');
INSERT INTO `schedule_job_log` VALUES ('349', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:52:10');
INSERT INTO `schedule_job_log` VALUES ('350', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:52:20');
INSERT INTO `schedule_job_log` VALUES ('351', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 13:52:30');
INSERT INTO `schedule_job_log` VALUES ('352', '3', 'testTask', 'test', 'renren', '0', null, '1015', '2016-12-20 13:52:40');
INSERT INTO `schedule_job_log` VALUES ('353', '3', 'testTask', 'test', 'renren', '0', null, '1073', '2016-12-20 17:07:42');
INSERT INTO `schedule_job_log` VALUES ('354', '3', 'testTask', 'test', 'renren', '0', null, '1020', '2016-12-20 17:07:50');
INSERT INTO `schedule_job_log` VALUES ('355', '3', 'testTask', 'test', 'renren', '0', null, '1040', '2016-12-20 17:08:00');
INSERT INTO `schedule_job_log` VALUES ('356', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:08:10');
INSERT INTO `schedule_job_log` VALUES ('357', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:08:20');
INSERT INTO `schedule_job_log` VALUES ('358', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:08:30');
INSERT INTO `schedule_job_log` VALUES ('359', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:08:40');
INSERT INTO `schedule_job_log` VALUES ('360', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:08:50');
INSERT INTO `schedule_job_log` VALUES ('361', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:09:00');
INSERT INTO `schedule_job_log` VALUES ('362', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:09:10');
INSERT INTO `schedule_job_log` VALUES ('363', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:09:20');
INSERT INTO `schedule_job_log` VALUES ('364', '3', 'testTask', 'test', 'renren', '0', null, '1016', '2016-12-20 17:09:30');
INSERT INTO `schedule_job_log` VALUES ('365', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:09:40');
INSERT INTO `schedule_job_log` VALUES ('366', '3', 'testTask', 'test', 'renren', '0', null, '1018', '2016-12-20 17:09:50');
INSERT INTO `schedule_job_log` VALUES ('367', '3', 'testTask', 'test', 'renren', '0', null, '1015', '2016-12-20 17:10:00');
INSERT INTO `schedule_job_log` VALUES ('368', '3', 'testTask', 'test', 'renren', '0', null, '1013', '2016-12-20 17:10:10');
INSERT INTO `schedule_job_log` VALUES ('369', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:10:20');
INSERT INTO `schedule_job_log` VALUES ('370', '3', 'testTask', 'test', 'renren', '0', null, '1022', '2016-12-20 17:10:30');
INSERT INTO `schedule_job_log` VALUES ('371', '3', 'testTask', 'test', 'renren', '0', null, '1015', '2016-12-20 17:10:40');
INSERT INTO `schedule_job_log` VALUES ('372', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:10:50');
INSERT INTO `schedule_job_log` VALUES ('373', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:11:00');
INSERT INTO `schedule_job_log` VALUES ('374', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:11:10');
INSERT INTO `schedule_job_log` VALUES ('375', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:11:20');
INSERT INTO `schedule_job_log` VALUES ('376', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:11:30');
INSERT INTO `schedule_job_log` VALUES ('377', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:11:40');
INSERT INTO `schedule_job_log` VALUES ('378', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:11:50');
INSERT INTO `schedule_job_log` VALUES ('379', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:12:00');
INSERT INTO `schedule_job_log` VALUES ('380', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:12:10');
INSERT INTO `schedule_job_log` VALUES ('381', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:12:20');
INSERT INTO `schedule_job_log` VALUES ('382', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:12:30');
INSERT INTO `schedule_job_log` VALUES ('383', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:12:40');
INSERT INTO `schedule_job_log` VALUES ('384', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:12:50');
INSERT INTO `schedule_job_log` VALUES ('385', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:13:00');
INSERT INTO `schedule_job_log` VALUES ('386', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:13:10');
INSERT INTO `schedule_job_log` VALUES ('387', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:13:20');
INSERT INTO `schedule_job_log` VALUES ('388', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:13:30');
INSERT INTO `schedule_job_log` VALUES ('389', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:13:40');
INSERT INTO `schedule_job_log` VALUES ('390', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:13:50');
INSERT INTO `schedule_job_log` VALUES ('391', '3', 'testTask', 'test', 'renren', '0', null, '1019', '2016-12-20 17:14:00');
INSERT INTO `schedule_job_log` VALUES ('392', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:14:10');
INSERT INTO `schedule_job_log` VALUES ('393', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-20 17:14:20');
INSERT INTO `schedule_job_log` VALUES ('394', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:14:30');
INSERT INTO `schedule_job_log` VALUES ('395', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:14:40');
INSERT INTO `schedule_job_log` VALUES ('396', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:14:50');
INSERT INTO `schedule_job_log` VALUES ('397', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:15:00');
INSERT INTO `schedule_job_log` VALUES ('398', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:15:10');
INSERT INTO `schedule_job_log` VALUES ('399', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:15:20');
INSERT INTO `schedule_job_log` VALUES ('400', '3', 'testTask', 'test', 'renren', '0', null, '1013', '2016-12-20 17:15:30');
INSERT INTO `schedule_job_log` VALUES ('401', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:15:40');
INSERT INTO `schedule_job_log` VALUES ('402', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:15:50');
INSERT INTO `schedule_job_log` VALUES ('403', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-20 17:16:00');
INSERT INTO `schedule_job_log` VALUES ('404', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:16:10');
INSERT INTO `schedule_job_log` VALUES ('405', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:16:20');
INSERT INTO `schedule_job_log` VALUES ('406', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:16:30');
INSERT INTO `schedule_job_log` VALUES ('407', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:16:40');
INSERT INTO `schedule_job_log` VALUES ('408', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:16:50');
INSERT INTO `schedule_job_log` VALUES ('409', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:17:00');
INSERT INTO `schedule_job_log` VALUES ('410', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:17:10');
INSERT INTO `schedule_job_log` VALUES ('411', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:17:20');
INSERT INTO `schedule_job_log` VALUES ('412', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:17:30');
INSERT INTO `schedule_job_log` VALUES ('413', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:17:40');
INSERT INTO `schedule_job_log` VALUES ('414', '3', 'testTask', 'test', 'renren', '0', null, '1003', '2016-12-20 17:17:50');
INSERT INTO `schedule_job_log` VALUES ('415', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:18:00');
INSERT INTO `schedule_job_log` VALUES ('416', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:18:10');
INSERT INTO `schedule_job_log` VALUES ('417', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:18:20');
INSERT INTO `schedule_job_log` VALUES ('418', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:18:30');
INSERT INTO `schedule_job_log` VALUES ('419', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:18:40');
INSERT INTO `schedule_job_log` VALUES ('420', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:18:50');
INSERT INTO `schedule_job_log` VALUES ('421', '3', 'testTask', 'test', 'renren', '0', null, '1019', '2016-12-20 17:19:00');
INSERT INTO `schedule_job_log` VALUES ('422', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:19:10');
INSERT INTO `schedule_job_log` VALUES ('423', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:19:20');
INSERT INTO `schedule_job_log` VALUES ('424', '3', 'testTask', 'test', 'renren', '0', null, '1107', '2016-12-20 17:19:30');
INSERT INTO `schedule_job_log` VALUES ('425', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-20 17:19:40');
INSERT INTO `schedule_job_log` VALUES ('426', '3', 'testTask', 'test', 'renren', '0', null, '1032', '2016-12-20 17:19:50');
INSERT INTO `schedule_job_log` VALUES ('427', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:20:00');
INSERT INTO `schedule_job_log` VALUES ('428', '3', 'testTask', 'test', 'renren', '0', null, '1037', '2016-12-20 17:20:11');
INSERT INTO `schedule_job_log` VALUES ('429', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:20:20');
INSERT INTO `schedule_job_log` VALUES ('430', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:20:30');
INSERT INTO `schedule_job_log` VALUES ('431', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:20:40');
INSERT INTO `schedule_job_log` VALUES ('432', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:20:50');
INSERT INTO `schedule_job_log` VALUES ('433', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:21:00');
INSERT INTO `schedule_job_log` VALUES ('434', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:21:10');
INSERT INTO `schedule_job_log` VALUES ('435', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:21:20');
INSERT INTO `schedule_job_log` VALUES ('436', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:21:30');
INSERT INTO `schedule_job_log` VALUES ('437', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:21:40');
INSERT INTO `schedule_job_log` VALUES ('438', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-20 17:21:50');
INSERT INTO `schedule_job_log` VALUES ('439', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-20 17:22:00');
INSERT INTO `schedule_job_log` VALUES ('440', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 17:22:10');
INSERT INTO `schedule_job_log` VALUES ('441', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:22:20');
INSERT INTO `schedule_job_log` VALUES ('442', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:22:30');
INSERT INTO `schedule_job_log` VALUES ('443', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:22:40');
INSERT INTO `schedule_job_log` VALUES ('444', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:22:50');
INSERT INTO `schedule_job_log` VALUES ('445', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:23:00');
INSERT INTO `schedule_job_log` VALUES ('446', '3', 'testTask', 'test', 'renren', '0', null, '1022', '2016-12-20 17:23:10');
INSERT INTO `schedule_job_log` VALUES ('447', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:23:20');
INSERT INTO `schedule_job_log` VALUES ('448', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:23:30');
INSERT INTO `schedule_job_log` VALUES ('449', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:23:40');
INSERT INTO `schedule_job_log` VALUES ('450', '3', 'testTask', 'test', 'renren', '0', null, '1019', '2016-12-20 17:23:50');
INSERT INTO `schedule_job_log` VALUES ('451', '3', 'testTask', 'test', 'renren', '0', null, '1019', '2016-12-20 17:24:00');
INSERT INTO `schedule_job_log` VALUES ('452', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:24:10');
INSERT INTO `schedule_job_log` VALUES ('453', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:24:20');
INSERT INTO `schedule_job_log` VALUES ('454', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:24:30');
INSERT INTO `schedule_job_log` VALUES ('455', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:24:40');
INSERT INTO `schedule_job_log` VALUES ('456', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:24:50');
INSERT INTO `schedule_job_log` VALUES ('457', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:25:00');
INSERT INTO `schedule_job_log` VALUES ('458', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:25:10');
INSERT INTO `schedule_job_log` VALUES ('459', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 17:25:20');
INSERT INTO `schedule_job_log` VALUES ('460', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:25:30');
INSERT INTO `schedule_job_log` VALUES ('461', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:25:40');
INSERT INTO `schedule_job_log` VALUES ('462', '3', 'testTask', 'test', 'renren', '0', null, '1018', '2016-12-20 17:27:14');
INSERT INTO `schedule_job_log` VALUES ('463', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:27:20');
INSERT INTO `schedule_job_log` VALUES ('464', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:27:30');
INSERT INTO `schedule_job_log` VALUES ('465', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:27:40');
INSERT INTO `schedule_job_log` VALUES ('466', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:27:50');
INSERT INTO `schedule_job_log` VALUES ('467', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:28:00');
INSERT INTO `schedule_job_log` VALUES ('468', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:28:10');
INSERT INTO `schedule_job_log` VALUES ('469', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-20 17:28:20');
INSERT INTO `schedule_job_log` VALUES ('470', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:28:30');
INSERT INTO `schedule_job_log` VALUES ('471', '3', 'testTask', 'test', 'renren', '0', null, '1034', '2016-12-20 17:28:40');
INSERT INTO `schedule_job_log` VALUES ('472', '3', 'testTask', 'test', 'renren', '0', null, '1013', '2016-12-20 17:28:50');
INSERT INTO `schedule_job_log` VALUES ('473', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:29:00');
INSERT INTO `schedule_job_log` VALUES ('474', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:29:11');
INSERT INTO `schedule_job_log` VALUES ('475', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:29:20');
INSERT INTO `schedule_job_log` VALUES ('476', '3', 'testTask', 'test', 'renren', '0', null, '1023', '2016-12-20 17:29:30');
INSERT INTO `schedule_job_log` VALUES ('477', '3', 'testTask', 'test', 'renren', '0', null, '1017', '2016-12-20 17:29:40');
INSERT INTO `schedule_job_log` VALUES ('478', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:29:50');
INSERT INTO `schedule_job_log` VALUES ('479', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('480', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:30:10');
INSERT INTO `schedule_job_log` VALUES ('481', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:30:20');
INSERT INTO `schedule_job_log` VALUES ('482', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:30:30');
INSERT INTO `schedule_job_log` VALUES ('483', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:30:40');
INSERT INTO `schedule_job_log` VALUES ('484', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:30:50');
INSERT INTO `schedule_job_log` VALUES ('485', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:31:00');
INSERT INTO `schedule_job_log` VALUES ('486', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:31:10');
INSERT INTO `schedule_job_log` VALUES ('487', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:31:20');
INSERT INTO `schedule_job_log` VALUES ('488', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:31:30');
INSERT INTO `schedule_job_log` VALUES ('489', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:31:40');
INSERT INTO `schedule_job_log` VALUES ('490', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:31:50');
INSERT INTO `schedule_job_log` VALUES ('491', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:32:00');
INSERT INTO `schedule_job_log` VALUES ('492', '3', 'testTask', 'test', 'renren', '0', null, '1013', '2016-12-20 17:32:10');
INSERT INTO `schedule_job_log` VALUES ('493', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:32:20');
INSERT INTO `schedule_job_log` VALUES ('494', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-20 17:32:30');
INSERT INTO `schedule_job_log` VALUES ('495', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:32:40');
INSERT INTO `schedule_job_log` VALUES ('496', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:32:50');
INSERT INTO `schedule_job_log` VALUES ('497', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:33:00');
INSERT INTO `schedule_job_log` VALUES ('498', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:33:10');
INSERT INTO `schedule_job_log` VALUES ('499', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:33:20');
INSERT INTO `schedule_job_log` VALUES ('500', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:33:30');
INSERT INTO `schedule_job_log` VALUES ('501', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:33:40');
INSERT INTO `schedule_job_log` VALUES ('502', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:33:50');
INSERT INTO `schedule_job_log` VALUES ('503', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:34:00');
INSERT INTO `schedule_job_log` VALUES ('504', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:34:10');
INSERT INTO `schedule_job_log` VALUES ('505', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:34:20');
INSERT INTO `schedule_job_log` VALUES ('506', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:34:30');
INSERT INTO `schedule_job_log` VALUES ('507', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:34:40');
INSERT INTO `schedule_job_log` VALUES ('508', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:34:50');
INSERT INTO `schedule_job_log` VALUES ('509', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:35:00');
INSERT INTO `schedule_job_log` VALUES ('510', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:35:10');
INSERT INTO `schedule_job_log` VALUES ('511', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:35:20');
INSERT INTO `schedule_job_log` VALUES ('512', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:35:30');
INSERT INTO `schedule_job_log` VALUES ('513', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:35:40');
INSERT INTO `schedule_job_log` VALUES ('514', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:35:50');
INSERT INTO `schedule_job_log` VALUES ('515', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:36:00');
INSERT INTO `schedule_job_log` VALUES ('516', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:36:10');
INSERT INTO `schedule_job_log` VALUES ('517', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:36:20');
INSERT INTO `schedule_job_log` VALUES ('518', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:36:30');
INSERT INTO `schedule_job_log` VALUES ('519', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:36:40');
INSERT INTO `schedule_job_log` VALUES ('520', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:36:50');
INSERT INTO `schedule_job_log` VALUES ('521', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:37:00');
INSERT INTO `schedule_job_log` VALUES ('522', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:37:10');
INSERT INTO `schedule_job_log` VALUES ('523', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:37:21');
INSERT INTO `schedule_job_log` VALUES ('524', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:37:30');
INSERT INTO `schedule_job_log` VALUES ('525', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:37:40');
INSERT INTO `schedule_job_log` VALUES ('526', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:37:50');
INSERT INTO `schedule_job_log` VALUES ('527', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:38:00');
INSERT INTO `schedule_job_log` VALUES ('528', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:38:10');
INSERT INTO `schedule_job_log` VALUES ('529', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:38:20');
INSERT INTO `schedule_job_log` VALUES ('530', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:38:30');
INSERT INTO `schedule_job_log` VALUES ('531', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:38:40');
INSERT INTO `schedule_job_log` VALUES ('532', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-20 17:38:50');
INSERT INTO `schedule_job_log` VALUES ('533', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:39:00');
INSERT INTO `schedule_job_log` VALUES ('534', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:39:10');
INSERT INTO `schedule_job_log` VALUES ('535', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:39:20');
INSERT INTO `schedule_job_log` VALUES ('536', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:39:30');
INSERT INTO `schedule_job_log` VALUES ('537', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:39:40');
INSERT INTO `schedule_job_log` VALUES ('538', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-20 17:39:50');
INSERT INTO `schedule_job_log` VALUES ('539', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:40:00');
INSERT INTO `schedule_job_log` VALUES ('540', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:40:10');
INSERT INTO `schedule_job_log` VALUES ('541', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:40:20');
INSERT INTO `schedule_job_log` VALUES ('542', '3', 'testTask', 'test', 'renren', '0', null, '1003', '2016-12-20 17:40:30');
INSERT INTO `schedule_job_log` VALUES ('543', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:40:40');
INSERT INTO `schedule_job_log` VALUES ('544', '3', 'testTask', 'test', 'renren', '0', null, '1003', '2016-12-20 17:40:50');
INSERT INTO `schedule_job_log` VALUES ('545', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:41:00');
INSERT INTO `schedule_job_log` VALUES ('546', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:41:10');
INSERT INTO `schedule_job_log` VALUES ('547', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:41:20');
INSERT INTO `schedule_job_log` VALUES ('548', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:41:30');
INSERT INTO `schedule_job_log` VALUES ('549', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:41:40');
INSERT INTO `schedule_job_log` VALUES ('550', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:41:50');
INSERT INTO `schedule_job_log` VALUES ('551', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:42:00');
INSERT INTO `schedule_job_log` VALUES ('552', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:42:10');
INSERT INTO `schedule_job_log` VALUES ('553', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:42:20');
INSERT INTO `schedule_job_log` VALUES ('554', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:42:30');
INSERT INTO `schedule_job_log` VALUES ('555', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:42:40');
INSERT INTO `schedule_job_log` VALUES ('556', '3', 'testTask', 'test', 'renren', '0', null, '1020', '2016-12-20 17:42:50');
INSERT INTO `schedule_job_log` VALUES ('557', '3', 'testTask', 'test', 'renren', '0', null, '1003', '2016-12-20 17:43:00');
INSERT INTO `schedule_job_log` VALUES ('558', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:43:10');
INSERT INTO `schedule_job_log` VALUES ('559', '3', 'testTask', 'test', 'renren', '0', null, '1026', '2016-12-20 17:43:20');
INSERT INTO `schedule_job_log` VALUES ('560', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:43:30');
INSERT INTO `schedule_job_log` VALUES ('561', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:43:40');
INSERT INTO `schedule_job_log` VALUES ('562', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:43:50');
INSERT INTO `schedule_job_log` VALUES ('563', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:44:00');
INSERT INTO `schedule_job_log` VALUES ('564', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:44:10');
INSERT INTO `schedule_job_log` VALUES ('565', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:44:20');
INSERT INTO `schedule_job_log` VALUES ('566', '3', 'testTask', 'test', 'renren', '0', null, '1021', '2016-12-20 17:44:30');
INSERT INTO `schedule_job_log` VALUES ('567', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:44:40');
INSERT INTO `schedule_job_log` VALUES ('568', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:44:50');
INSERT INTO `schedule_job_log` VALUES ('569', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:45:00');
INSERT INTO `schedule_job_log` VALUES ('570', '3', 'testTask', 'test', 'renren', '0', null, '1029', '2016-12-20 17:45:10');
INSERT INTO `schedule_job_log` VALUES ('571', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:45:20');
INSERT INTO `schedule_job_log` VALUES ('572', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:45:30');
INSERT INTO `schedule_job_log` VALUES ('573', '3', 'testTask', 'test', 'renren', '0', null, '1028', '2016-12-20 17:45:40');
INSERT INTO `schedule_job_log` VALUES ('574', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-20 17:45:50');
INSERT INTO `schedule_job_log` VALUES ('575', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:46:00');
INSERT INTO `schedule_job_log` VALUES ('576', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:46:10');
INSERT INTO `schedule_job_log` VALUES ('577', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:46:20');
INSERT INTO `schedule_job_log` VALUES ('578', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:46:30');
INSERT INTO `schedule_job_log` VALUES ('579', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:46:40');
INSERT INTO `schedule_job_log` VALUES ('580', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:46:50');
INSERT INTO `schedule_job_log` VALUES ('581', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:47:00');
INSERT INTO `schedule_job_log` VALUES ('582', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:47:10');
INSERT INTO `schedule_job_log` VALUES ('583', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-20 17:47:20');
INSERT INTO `schedule_job_log` VALUES ('584', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:47:30');
INSERT INTO `schedule_job_log` VALUES ('585', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:47:40');
INSERT INTO `schedule_job_log` VALUES ('586', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-20 17:47:50');
INSERT INTO `schedule_job_log` VALUES ('587', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:48:00');
INSERT INTO `schedule_job_log` VALUES ('588', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-20 17:48:10');
INSERT INTO `schedule_job_log` VALUES ('589', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-20 17:48:20');
INSERT INTO `schedule_job_log` VALUES ('590', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-20 17:48:30');
INSERT INTO `schedule_job_log` VALUES ('591', '3', 'testTask', 'test', 'renren', '0', null, '1104', '2016-12-22 09:26:51');
INSERT INTO `schedule_job_log` VALUES ('592', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:26:52');
INSERT INTO `schedule_job_log` VALUES ('593', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-22 09:27:00');
INSERT INTO `schedule_job_log` VALUES ('594', '3', 'testTask', 'test', 'renren', '0', null, '1016', '2016-12-22 09:27:10');
INSERT INTO `schedule_job_log` VALUES ('595', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-22 09:27:20');
INSERT INTO `schedule_job_log` VALUES ('596', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-22 09:27:30');
INSERT INTO `schedule_job_log` VALUES ('597', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:27:40');
INSERT INTO `schedule_job_log` VALUES ('598', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-22 09:27:50');
INSERT INTO `schedule_job_log` VALUES ('599', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-22 09:28:00');
INSERT INTO `schedule_job_log` VALUES ('600', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-22 09:28:10');
INSERT INTO `schedule_job_log` VALUES ('601', '3', 'testTask', 'test', 'renren', '0', null, '1019', '2016-12-22 09:28:20');
INSERT INTO `schedule_job_log` VALUES ('602', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-22 09:28:31');
INSERT INTO `schedule_job_log` VALUES ('603', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-22 09:28:40');
INSERT INTO `schedule_job_log` VALUES ('604', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-22 09:28:50');
INSERT INTO `schedule_job_log` VALUES ('605', '3', 'testTask', 'test', 'renren', '0', null, '1022', '2016-12-22 09:29:00');
INSERT INTO `schedule_job_log` VALUES ('606', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:29:10');
INSERT INTO `schedule_job_log` VALUES ('607', '3', 'testTask', 'test', 'renren', '0', null, '1003', '2016-12-22 09:29:20');
INSERT INTO `schedule_job_log` VALUES ('608', '3', 'testTask', 'test', 'renren', '0', null, '1024', '2016-12-22 09:30:45');
INSERT INTO `schedule_job_log` VALUES ('609', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:30:51');
INSERT INTO `schedule_job_log` VALUES ('610', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:31:00');
INSERT INTO `schedule_job_log` VALUES ('611', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-22 09:31:10');
INSERT INTO `schedule_job_log` VALUES ('612', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-22 09:31:20');
INSERT INTO `schedule_job_log` VALUES ('613', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:31:30');
INSERT INTO `schedule_job_log` VALUES ('614', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-22 09:31:40');
INSERT INTO `schedule_job_log` VALUES ('615', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:31:50');
INSERT INTO `schedule_job_log` VALUES ('616', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:32:00');
INSERT INTO `schedule_job_log` VALUES ('617', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:32:10');
INSERT INTO `schedule_job_log` VALUES ('618', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:32:20');
INSERT INTO `schedule_job_log` VALUES ('619', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-22 09:32:30');
INSERT INTO `schedule_job_log` VALUES ('620', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-22 09:32:40');
INSERT INTO `schedule_job_log` VALUES ('621', '3', 'testTask', 'test', 'renren', '0', null, '1027', '2016-12-22 09:32:51');
INSERT INTO `schedule_job_log` VALUES ('622', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:33:01');
INSERT INTO `schedule_job_log` VALUES ('623', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 09:33:10');
INSERT INTO `schedule_job_log` VALUES ('624', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-22 09:33:20');
INSERT INTO `schedule_job_log` VALUES ('625', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-22 09:33:30');
INSERT INTO `schedule_job_log` VALUES ('626', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:33:40');
INSERT INTO `schedule_job_log` VALUES ('627', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-22 09:33:50');
INSERT INTO `schedule_job_log` VALUES ('628', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-22 09:34:00');
INSERT INTO `schedule_job_log` VALUES ('629', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-22 09:34:11');
INSERT INTO `schedule_job_log` VALUES ('630', '3', 'testTask', 'test', 'renren', '0', null, '1006', '2016-12-22 09:34:20');
INSERT INTO `schedule_job_log` VALUES ('631', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-22 09:34:31');
INSERT INTO `schedule_job_log` VALUES ('632', '3', 'testTask', 'test', 'renren', '0', null, '1005', '2016-12-22 09:34:41');
INSERT INTO `schedule_job_log` VALUES ('633', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-22 09:34:50');
INSERT INTO `schedule_job_log` VALUES ('634', '3', 'testTask', 'test', 'renren', '0', null, '1120', '2016-12-22 23:22:10');
INSERT INTO `schedule_job_log` VALUES ('635', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 23:22:11');
INSERT INTO `schedule_job_log` VALUES ('636', '3', 'testTask', 'test', 'renren', '0', null, '1015', '2016-12-22 23:22:20');
INSERT INTO `schedule_job_log` VALUES ('637', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-22 23:22:31');
INSERT INTO `schedule_job_log` VALUES ('638', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-22 23:22:40');
INSERT INTO `schedule_job_log` VALUES ('639', '3', 'testTask', 'test', 'renren', '0', null, '1004', '2016-12-22 23:22:50');
INSERT INTO `schedule_job_log` VALUES ('640', '3', 'testTask', 'test', 'renren', '0', null, '1037', '2016-12-26 23:28:17');
INSERT INTO `schedule_job_log` VALUES ('641', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:28:21');
INSERT INTO `schedule_job_log` VALUES ('642', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-26 23:28:30');
INSERT INTO `schedule_job_log` VALUES ('643', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-26 23:28:40');
INSERT INTO `schedule_job_log` VALUES ('644', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-26 23:28:50');
INSERT INTO `schedule_job_log` VALUES ('645', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:29:00');
INSERT INTO `schedule_job_log` VALUES ('646', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-26 23:29:10');
INSERT INTO `schedule_job_log` VALUES ('647', '3', 'testTask', 'test', 'renren', '0', null, '1040', '2016-12-26 23:29:20');
INSERT INTO `schedule_job_log` VALUES ('648', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:29:30');
INSERT INTO `schedule_job_log` VALUES ('649', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-26 23:29:40');
INSERT INTO `schedule_job_log` VALUES ('650', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-26 23:29:50');
INSERT INTO `schedule_job_log` VALUES ('651', '3', 'testTask', 'test', 'renren', '0', null, '11288', '2016-12-26 23:33:19');
INSERT INTO `schedule_job_log` VALUES ('652', '3', 'testTask', 'test', 'renren', '0', null, '3880', '2016-12-26 23:33:31');
INSERT INTO `schedule_job_log` VALUES ('653', '3', 'testTask', 'test', 'renren', '0', null, '12814', '2016-12-26 23:33:20');
INSERT INTO `schedule_job_log` VALUES ('654', '3', 'testTask', 'test', 'renren', '0', null, '5195', '2016-12-26 23:33:40');
INSERT INTO `schedule_job_log` VALUES ('655', '3', 'testTask', 'test', 'renren', '0', null, '3270', '2016-12-26 23:33:50');
INSERT INTO `schedule_job_log` VALUES ('656', '3', 'testTask', 'test', 'renren', '0', null, '1016', '2016-12-26 23:34:00');
INSERT INTO `schedule_job_log` VALUES ('657', '3', 'testTask', 'test', 'renren', '0', null, '1013', '2016-12-26 23:34:10');
INSERT INTO `schedule_job_log` VALUES ('658', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:34:20');
INSERT INTO `schedule_job_log` VALUES ('659', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-26 23:34:30');
INSERT INTO `schedule_job_log` VALUES ('660', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:34:40');
INSERT INTO `schedule_job_log` VALUES ('661', '3', 'testTask', 'test', 'renren', '0', null, '1015', '2016-12-26 23:34:50');
INSERT INTO `schedule_job_log` VALUES ('662', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-26 23:35:00');
INSERT INTO `schedule_job_log` VALUES ('663', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:35:10');
INSERT INTO `schedule_job_log` VALUES ('664', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:35:20');
INSERT INTO `schedule_job_log` VALUES ('665', '3', 'testTask', 'test', 'renren', '0', null, '1018', '2016-12-26 23:35:30');
INSERT INTO `schedule_job_log` VALUES ('666', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:35:40');
INSERT INTO `schedule_job_log` VALUES ('667', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:35:51');
INSERT INTO `schedule_job_log` VALUES ('668', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:36:00');
INSERT INTO `schedule_job_log` VALUES ('669', '3', 'testTask', 'test', 'renren', '0', null, '1017', '2016-12-26 23:36:10');
INSERT INTO `schedule_job_log` VALUES ('670', '3', 'testTask', 'test', 'renren', '0', null, '1012', '2016-12-26 23:36:20');
INSERT INTO `schedule_job_log` VALUES ('671', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:36:30');
INSERT INTO `schedule_job_log` VALUES ('672', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:36:40');
INSERT INTO `schedule_job_log` VALUES ('673', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:36:51');
INSERT INTO `schedule_job_log` VALUES ('674', '3', 'testTask', 'test', 'renren', '0', null, '1020', '2016-12-26 23:37:00');
INSERT INTO `schedule_job_log` VALUES ('675', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:37:10');
INSERT INTO `schedule_job_log` VALUES ('676', '3', 'testTask', 'test', 'renren', '0', null, '1019', '2016-12-26 23:37:20');
INSERT INTO `schedule_job_log` VALUES ('677', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:37:30');
INSERT INTO `schedule_job_log` VALUES ('678', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-26 23:37:41');
INSERT INTO `schedule_job_log` VALUES ('679', '3', 'testTask', 'test', 'renren', '0', null, '1008', '2016-12-26 23:37:51');
INSERT INTO `schedule_job_log` VALUES ('680', '3', 'testTask', 'test', 'renren', '0', null, '1009', '2016-12-26 23:38:00');
INSERT INTO `schedule_job_log` VALUES ('681', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:38:10');
INSERT INTO `schedule_job_log` VALUES ('682', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:38:20');
INSERT INTO `schedule_job_log` VALUES ('683', '3', 'testTask', 'test', 'renren', '0', null, '1017', '2016-12-26 23:38:30');
INSERT INTO `schedule_job_log` VALUES ('684', '3', 'testTask', 'test', 'renren', '0', null, '1014', '2016-12-26 23:38:40');
INSERT INTO `schedule_job_log` VALUES ('685', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:38:50');
INSERT INTO `schedule_job_log` VALUES ('686', '3', 'testTask', 'test', 'renren', '0', null, '2125', '2016-12-26 23:44:33');
INSERT INTO `schedule_job_log` VALUES ('687', '3', 'testTask', 'test', 'renren', '0', null, '1011', '2016-12-26 23:44:40');
INSERT INTO `schedule_job_log` VALUES ('688', '3', 'testTask', 'test', 'renren', '0', null, '1010', '2016-12-26 23:44:50');
INSERT INTO `schedule_job_log` VALUES ('689', '3', 'testTask', 'test', 'renren', '0', null, '1055', '2016-12-27 00:02:48');
INSERT INTO `schedule_job_log` VALUES ('690', '3', 'testTask', 'test', 'renren', '0', null, '1016', '2016-12-27 00:02:50');
INSERT INTO `schedule_job_log` VALUES ('691', '3', 'testTask', 'test', 'renren', '0', null, '1007', '2016-12-27 00:03:00');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL COMMENT 'key',
  `value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'yclx', '0', '1', '用车类型');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'fa fa-cog', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '管理员列表', 'sys/user.html', null, '1', 'fa fa-user', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'sys/role.html', null, '1', 'fa fa-user-secret', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'sys/menu.html', null, '1', 'fa fa-th-list', '3');
INSERT INTO `sys_menu` VALUES ('5', '1', 'SQL监控', 'druid/sql.html', null, '1', 'fa fa-bug', '4');
INSERT INTO `sys_menu` VALUES ('6', '1', '定时任务', 'sys/schedule.html', null, '1', 'fa fa-tasks', '5');
INSERT INTO `sys_menu` VALUES ('7', '6', '查看', null, 'sys:schedule:list,sys:schedule:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '6', '新增', null, 'sys:schedule:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('9', '6', '修改', null, 'sys:schedule:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '6', '删除', null, 'sys:schedule:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '6', '暂停', null, 'sys:schedule:pause', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '6', '恢复', null, 'sys:schedule:resume', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '6', '立即执行', null, 'sys:schedule:run', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '6', '日志列表', null, 'sys:schedule:log', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '参数管理', 'sys/config.html', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'fa fa-sun-o', '6');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '系统管理员', '全部菜单', '2016-12-15 17:38:03');
INSERT INTO `sys_role` VALUES ('2', '普通会员', '菜单比较少', '2016-12-15 17:56:06');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('3', '1', '15');
INSERT INTO `sys_role_menu` VALUES ('4', '1', '16');
INSERT INTO `sys_role_menu` VALUES ('5', '1', '17');
INSERT INTO `sys_role_menu` VALUES ('6', '1', '18');
INSERT INTO `sys_role_menu` VALUES ('7', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('8', '1', '19');
INSERT INTO `sys_role_menu` VALUES ('9', '1', '20');
INSERT INTO `sys_role_menu` VALUES ('10', '1', '21');
INSERT INTO `sys_role_menu` VALUES ('11', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('12', '1', '4');
INSERT INTO `sys_role_menu` VALUES ('13', '1', '23');
INSERT INTO `sys_role_menu` VALUES ('14', '1', '24');
INSERT INTO `sys_role_menu` VALUES ('15', '1', '25');
INSERT INTO `sys_role_menu` VALUES ('16', '1', '26');
INSERT INTO `sys_role_menu` VALUES ('17', '1', '5');
INSERT INTO `sys_role_menu` VALUES ('18', '1', '6');
INSERT INTO `sys_role_menu` VALUES ('19', '1', '7');
INSERT INTO `sys_role_menu` VALUES ('20', '1', '8');
INSERT INTO `sys_role_menu` VALUES ('21', '1', '9');
INSERT INTO `sys_role_menu` VALUES ('22', '1', '10');
INSERT INTO `sys_role_menu` VALUES ('23', '1', '11');
INSERT INTO `sys_role_menu` VALUES ('24', '1', '12');
INSERT INTO `sys_role_menu` VALUES ('25', '1', '13');
INSERT INTO `sys_role_menu` VALUES ('26', '1', '14');
INSERT INTO `sys_role_menu` VALUES ('27', '1', '27');
INSERT INTO `sys_role_menu` VALUES ('28', '2', '1');
INSERT INTO `sys_role_menu` VALUES ('29', '2', '2');
INSERT INTO `sys_role_menu` VALUES ('30', '2', '15');
INSERT INTO `sys_role_menu` VALUES ('31', '2', '3');
INSERT INTO `sys_role_menu` VALUES ('32', '2', '19');
INSERT INTO `sys_role_menu` VALUES ('33', '2', '5');
INSERT INTO `sys_role_menu` VALUES ('34', '2', '6');
INSERT INTO `sys_role_menu` VALUES ('35', '2', '7');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9', 'root@renren.io', '13612345678', '1', '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES ('3', 'root', 'e14cb9e5c0eeee0ea313a4e04fbd10aa17ac17aa33a3cad4bdfe74b87ca18ef8', '962155660@qq.com', '13083494398', '1', '2016-12-15 17:39:16');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '3', '1');
