package com.df.renren.controller;

import com.df.renren.entity.SysUserEntity;
import com.df.renren.utils.Constant;
import com.df.renren.utils.EhcacheUtil;
import com.df.renren.utils.ShiroUtils;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller公共组件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月9日 下午9:42:26
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	public HttpServletRequest req;
	
	protected SysUserEntity getUser() {
		return ShiroUtils.getUserEntity();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}
	
	protected SysUserEntity getAppUser(){
		String _token = req.getParameter("token");
		if(_token == null){
			_token = req.getHeader("token");
		}
		EhcacheUtil util = EhcacheUtil.getInstance();
		SysUserEntity user = (SysUserEntity) util.get(Constant.APP_TOKEN_CACHENAME, _token);
		return user;
	}
}
