package com.df.renren.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.df.renren.entity.CompanyEntity;
import com.df.renren.entity.SysConfigEntity;
import com.df.renren.entity.SysUserEntity;
import com.df.renren.service.CompanyService;
import com.df.renren.utils.PageUtils;
import com.df.renren.utils.R;

@RestController
@RequestMapping("/sys/company")
public class SysCompanyController extends AbstractController{

	@Autowired
	private CompanyService companyService;
	
	/**
	 * 所有用户列表
	 */
	@RequestMapping("/list")
	@RequiresPermissions("sys:company:list")
	public R list(Integer page, Integer limit){
		Page<CompanyEntity> _page = new Page<CompanyEntity>(page, limit);
		Page<CompanyEntity> pager = companyService.selectPage(_page);
		PageUtils pageUtil = new PageUtils(pager.getRecords(), pager.getTotal(), limit, page);
		return R.ok().put("page", pageUtil);
	}
	
	/**
	 * 配置信息
	 */
	@RequestMapping("/info/{id}")
	@RequiresPermissions("sys:company:info")
	public R info(@PathVariable("id") Long id){
		CompanyEntity company = companyService.selectById(id);
		
		return R.ok().put("company", company);
	}
}
