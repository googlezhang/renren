package com.df.renren.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.df.renren.entity.SysUserEntity;
import com.df.renren.service.FileService;
import com.df.renren.service.SysUserService;
import com.df.renren.service.impl.FileServiceImpl;
import com.df.renren.service.impl.SysUserServiceImpl;
import com.df.renren.utils.SpringContextUtils;


/**
 * Handles requests for the application file upload.
 */
@RestController
public class FileTransController  extends AbstractController{

    private static final Logger logger = LoggerFactory.getLogger(FileTransController.class);

    @Autowired
    private FileService fileService;
    
    @RequestMapping(value = "/uploadPic", method = RequestMethod.POST)
    public Map<String,Object> uploadPic(HttpServletRequest request,HttpServletResponse response)throws UnsupportedEncodingException,  
    FileUploadException, IOException {
    	super.req = request;
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) req;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        String realPath = req.getServletContext().getRealPath("/");
        String webPath = realPath.replace("\\renren\\", "");
        String urls = "";
        for (Entry<String, MultipartFile> e : fileMap.entrySet()) {
            urls += fileService.uploadImage(e.getValue(),webPath);
        }
        
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("avatarUrls",urls);
        map.put("success",true );
        map.put("msg","Success!");
        return map;
    }
    
   public static void main(String[] args){
	   String str = "D:\\soft\\apache-tomcat-8.0.39\\webapps\\renren\\";
	   System.out.println(str.replace("\\renren\\", ""));
	  
	 
   }

    @RequestMapping("/preview")
    public void preview(@RequestParam(value = "url", required = true) String url,
            HttpServletRequest request, HttpServletResponse response) {
        File file = fileService.getImageFile(url);
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            OutputStream os = response.getOutputStream();
            byte[] bt = new byte[1024];
            int len = -1;
            while ((len = is.read(bt)) != -1) {
                os.write(bt, 0, len);
            }
            response.setContentType("application/png");
            os.write("\r\n".getBytes());
            os.flush();
            os.close();
            is.close();
        } catch (FileNotFoundException e) {
            logger.error("图片丢失");
        } catch (IOException e) {
            logger.error("图片预览发生 IOException");
        }

    }

    @RequestMapping(value = "/deletePic", method = RequestMethod.POST)
    @ResponseBody
    public String deletePic(@RequestParam(value = "url", required = true) String url,
            HttpServletRequest req) throws IOException {
        String[] urls = url.split(";");
        fileService.deleteImage(Arrays.asList(urls));
        return "success";
    }

}
