package com.df.renren.vo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;


public class UserAttentionInfoVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long uid;
	private Long auid;
	
	private String username;
	private String mobile;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Long getAuid() {
		return auid;
	}
	public void setAuid(Long auid) {
		this.auid = auid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
