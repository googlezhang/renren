package com.df.renren.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.df.renren.entity.CompanyEntity;
import com.df.renren.entity.DynamicEntity;

/**
 * 动态管理接口
 * @author cdf
 *
 */
public interface DynamicService extends IService<DynamicEntity>{
	
	List<Map<String, Object>> findById(Long id,Integer page,Integer limit);


}
