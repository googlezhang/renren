package com.df.renren.service;

import java.io.File;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;


public interface FileService {
    
    /**
     * 
     * @param url
     * @return
     */
    File getImageFile(String url);


    /**
     * Upload image to local
     * 
     * @param multipartFile
     * @return file path
     */
    String uploadImage(MultipartFile multipartFile, String realPath);


    /**
     * delete image;
     * 
     * @param imageUrl
     */
    void deleteImage(String imageUrl);

    /**
     * delete images
     * 
     * @param imageUrls
     */
    void deleteImage(List<String> imageUrls);

}
