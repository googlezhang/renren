package com.df.renren.service;

import java.util.List;

/**
 * jedis管理接口
 * @author cdf
 *
 */
public interface JedisService {

	String set(String key, Object obj);

	String setList(String key, List value);
	
	Boolean exists(String key);

	/**
	 * 设置单个值和有效时间
	 * @param key
	 * @param value
	 * @param seconds
	 * @return
	 */
	String set(String key, Object value, int seconds);

	/**
	 * 设置List和有效时间
	 * @param key
	 * @param value
	 * @param seconds
	 * @return
	 */
	String setList(String key, List value, int seconds);

	/**
	 * 获取单个值
	 * @param key
	 * @param clazz
	 * @return
	 */
	<T> T get(String key, Class<T> clazz);

	/**
	 * 获取List 
	 * @param key
	 * @param clazz
	 * @return
	 */
	<T> List<T> getList(String key, Class<T> clazz);
	
	/**
	 * 设置key的有效持续时间
	 * @param key
	 * @param seconds
	 * @return
	 */
	Long expire(String key,int seconds);
	
	/**
	 * 设置key的过期点
	 * @param key
	 * @param unixTime
	 * @return
	 */
	Long expire(String key,long unixTime);

}
