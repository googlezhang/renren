package com.df.renren.service;

import com.baomidou.mybatisplus.service.IService;

/**
 * baseService
 * 
 * @author cdf
 *
 * @param <T>
 */
public interface BaseService<T> extends IService<T>{

}
