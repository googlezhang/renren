package com.df.renren.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.df.renren.service.JedisService;
import com.df.renren.utils.ProtobuffSerializationUtil;

import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

@Service("jedisService")
public class JedisServiceImpl implements JedisService {

	private static final Logger log = Logger.getLogger(JedisServiceImpl.class);  
	
	@Autowired
	private ShardedJedisPool  shardedJedisPool;

	@Override
	public String set(String key, Object obj) {
		String result = null;
		ShardedJedis shardedJedis = shardedJedisPool.getResource();
		if (shardedJedis == null) {
			return result;
		}
		try {
			result = shardedJedis.set(key, new String(ProtobuffSerializationUtil.serialize(obj), "ISO-8859-1"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			shardedJedis.close();
		}
		return result;
	}

	@Override
	public String setList(String key, List value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean exists(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String set(String key, Object value, int seconds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String setList(String key, List value, int seconds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T get(String key, Class<T> clazz) {
		  ShardedJedis shardedJedis = shardedJedisPool.getResource();  
	        if(shardedJedis == null){  
	            return null;  
	        }  
	        try {  
	            String resultStr = shardedJedis.get(key);  
	            if(StringUtils.isEmpty(resultStr))  
	                return null;  
	            return ProtobuffSerializationUtil.deserialize(resultStr.getBytes("ISO-8859-1"), clazz);  
	        } catch (Exception e) {  
	            log.error(e.getMessage(),e);  
	            e.printStackTrace();  
	        } finally{  
	            shardedJedis.close();  
	        }  
	        return null;  
	}

	@Override
	public <T> List<T> getList(String key, Class<T> clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long expire(String key, int seconds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long expire(String key, long unixTime) {
		// TODO Auto-generated method stub
		return null;
	}

}
