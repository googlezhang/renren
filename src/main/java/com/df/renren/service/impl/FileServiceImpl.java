package com.df.renren.service.impl;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.df.renren.service.FileService;




/**
 * Service - File
 */
@Service("fileService")
public class FileServiceImpl implements FileService {

	private static String UPLOAD_PATH_DEFAULT = "/upload/image/";
	
    /**
     * 获取图片流
     * 
     * @param url
     * @return
     */
    public File getImageFile(String url) {
        return new File(url);
    }

    /**
     * 上传图片，获取上传后的图片路径
     * @param realPath 
     */
    public String uploadImage(MultipartFile multipartFile, String realPath) {
        if (multipartFile == null) {
            return null;
        }
        try {
            String filename =
                    UUID.randomUUID() + "."
                            + FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            
            String rePath = UPLOAD_PATH_DEFAULT + filename;
            String destPath = realPath + rePath;
            File destFile = new File(destPath);
            if (!destFile.getParentFile().exists()) {
                destFile.getParentFile().mkdirs();
            }
            multipartFile.transferTo(destFile);
            return rePath;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 删除图片
     */
    public void deleteImage(String imageUrl) {
        File destFile = new File(imageUrl);
        if (destFile.exists()) {
            destFile.delete();
        }
    }

    /**
     * 批量删除图片
     */
    public void deleteImage(List<String> imageUrls) {
        for (String url : imageUrls) {
            deleteImage(url);
        }
    }

    
  

}
