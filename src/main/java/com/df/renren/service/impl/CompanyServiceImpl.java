package com.df.renren.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.df.renren.dao.CompanyDao;
import com.df.renren.entity.CompanyEntity;
import com.df.renren.service.CompanyService;

@Service("companyService")
public class CompanyServiceImpl extends BaseServiceImpl<CompanyEntity> implements CompanyService {

	@Autowired
	private CompanyDao companyDao;

	@Override
	public List<CompanyEntity> findByPid(Long id) {
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("company_parent_id", id);
		return companyDao.selectByMap(columnMap);
	}
}
