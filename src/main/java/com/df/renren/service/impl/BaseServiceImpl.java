package com.df.renren.service.impl;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.df.renren.service.BaseService;

public class BaseServiceImpl<T> extends ServiceImpl<BaseMapper<T>, T> implements BaseService<T>{

}
