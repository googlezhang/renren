package com.df.renren.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.df.renren.dao.SysUserAttentionDao;
import com.df.renren.entity.UserAttentionEntity;
import com.df.renren.service.UserAttentionService;
import com.df.renren.vo.UserAttentionInfoVo;

@Service("userAttentionService")
public class UserAttentionServiceImpl extends BaseServiceImpl<UserAttentionEntity> implements UserAttentionService {

	@Autowired
	private SysUserAttentionDao sysuserattentiondao;

	@Override
	public List<Map<String, Object>> findByuid(Long id) {
		Map<String, Object> sysuserattMap = new HashMap<String, Object>();
		sysuserattMap.put("uid", id);
		String sql = "select auid,username,mobile,user_pic from sys_user s join sys_user_attention su on s.user_id = su.auid and su.uid="+id;
		
		return sysuserattentiondao.selectListSql(sql);
	}

	@Override
	public void addAttention(Long uid, Long auid) {
		// TODO Auto-generated method stub
		UserAttentionEntity suatt = new UserAttentionEntity();
		suatt.setUid(uid);
		suatt.setAuid(auid);
		sysuserattentiondao.insert(suatt);
	}

	@Override
	public void cancelAttention(Long uid, Long auid) {
		// TODO Auto-generated method stub
		String sql="delete from sys_user_attention where id ="+uid+"and auid = "+auid;
		sysuserattentiondao.deleteSql(sql);
	}

	//根据uid查找用户
	@Override
	public List<UserAttentionEntity> findUserByuid(Long uid) {
		Map<String,Object> idmap = new HashMap<String, Object>();
		idmap.put("uid", uid);
		return sysuserattentiondao.selectByMap(idmap);
	}
}
