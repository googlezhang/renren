package com.df.renren.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.df.renren.dao.DynamicDao;
import com.df.renren.entity.DynamicEntity;
import com.df.renren.service.DynamicService;

@Service("dynamicService")
public class DynamicServiceImpl extends BaseServiceImpl<DynamicEntity> implements DynamicService {

	@Autowired
	private DynamicDao dynamicDao;
	
	@Override
	public List<Map<String, Object>> findById(Long id,Integer page,Integer limit) {
		String sql = "select dynamic.content as content,dynamic.picture as picture,dynamic.create_time as createTime,sys_user_attention.auid as auid,sys_user.username as username from dynamic,sys_user"
				+ " join sys_user_attention where  dynamic.userid=sys_user.user_id and dynamic.userid=sys_user_attention.auid and sys_user_attention.uid="+id+" order by dynamic.create_time desc limit "+(page-1)*limit+","+limit;
		return dynamicDao.selectListSql(sql);
	}
}
