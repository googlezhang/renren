package com.df.renren.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.df.renren.entity.UserAttentionEntity;
import com.df.renren.vo.UserAttentionInfoVo;

/**
 * 单位管理接口
 * @author cdf
 *
 */
public interface UserAttentionService extends IService<UserAttentionEntity>{
	
	List<Map<String,Object>> findByuid(Long id);
	
	void addAttention(Long uid,Long auid);
	
	void cancelAttention(Long uid,Long auid);
	
	List<UserAttentionEntity> findUserByuid(Long uid);

}
