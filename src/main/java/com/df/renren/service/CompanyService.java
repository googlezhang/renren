package com.df.renren.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.df.renren.entity.CompanyEntity;

/**
 * 单位管理接口
 * @author cdf
 *
 */
public interface CompanyService extends IService<CompanyEntity>{
	
	List<CompanyEntity> findByPid(Long id);

}
