package com.df.renren.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.df.renren.entity.CompanyEntity;

public interface CompanyDao extends BaseMapper<CompanyEntity>{

}
