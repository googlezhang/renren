package com.df.renren.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.df.renren.entity.DynamicEntity;

public interface DynamicDao extends BaseMapper<DynamicEntity>{

}
