package com.df.renren.app.api;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.df.renren.controller.AbstractController;
import com.df.renren.entity.UserAttentionEntity;
import com.df.renren.entity.SysUserEntity;
import com.df.renren.service.UserAttentionService;
import com.df.renren.service.SysUserService;
import com.df.renren.utils.Constant;
import com.df.renren.utils.EhcacheUtil;
import com.df.renren.utils.EncryptDecryptUtil;
import com.df.renren.utils.PageUtils;
import com.df.renren.utils.R;

@RestController
@RequestMapping("/app/user")
public class AppUserController extends AbstractController{

	@Autowired
	private SysUserService userService;
	
	@Autowired
	private UserAttentionService sysuserattentionservice;

	@RequestMapping("/login")
	public R logincheck(String account, String password) throws Exception {
		password = new Sha256Hash(password).toHex();
		SysUserEntity user = userService.queryByUserName(account);
		if(user == null){
			return R.error(201, "该账户不存在");
		}
		if (!user.getPassword().equals(password)) {
			return R.error(201, "登陆失败,请输入正确密码");
		}
		// 校验token
		EncryptDecryptUtil edUtil = new EncryptDecryptUtil();
		String newToken = edUtil.encrypt(user.getUserId().toString() + "@" + System.currentTimeMillis());
		EhcacheUtil cacheUtile = EhcacheUtil.getInstance();
		List<String> list = cacheUtile.getKeys(Constant.APP_TOKEN_CACHENAME);
		for (String key : list) {
			String coverStr = edUtil.decrypt(key);
			String[] arr = coverStr.split("@");
			if (arr[0].equals(user.getUserId().toString())) {
				cacheUtile.remove(Constant.APP_TOKEN_CACHENAME, key);
			}
		}
		cacheUtile.put(Constant.APP_TOKEN_CACHENAME, newToken, user);
		return R.ok().put("data", newToken);
	}

	@RequestMapping("/getUser")
	public R getUserInfo() {
		SysUserEntity user = getAppUser();
		return R.ok().put("data", user);
	}
	
	@RequestMapping("/sayhello")
	public R sayHello(){
		return R.ok().put("data", "hello");
	}
	
	/**
	 * 所有关注的用户列表
	 */
	@RequestMapping("/getAttentions")
	public R list(){
		SysUserEntity user = getAppUser();
		List<Map<String, Object>> suattention = sysuserattentionservice.findByuid(user.getUserId());
	
		return R.ok().put("data",suattention);
	}
	
	/**
	 * 根据id查询
	 */
	@RequestMapping("/getAttentionById")
	public R info(Long id){
		UserAttentionEntity suattention = sysuserattentionservice.selectById(id);
		return R.ok().put("sysuserattention", suattention);
	}
	
	
	
	/**
	 * 加关注
	 * @param uid
	 * @param auid
	 */
	@RequestMapping("/addAttention")
	public R addAttention(@PathVariable("uid") Long uid,@PathVariable("auid") Long auid){
	    sysuserattentionservice.addAttention(uid, auid);
	    return R.ok();
	}
	
	/**
	 * 取消关注
	 * @param uid
	 * @param auid
	 */
	@RequestMapping("/cancelAttention")
	public R cancelAttention(@PathVariable("uid") Long uid,@PathVariable("auid") Long auid){
	    sysuserattentionservice.cancelAttention(uid, auid);
	    return R.ok();
	}

}
