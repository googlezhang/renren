package com.df.renren.app.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.df.renren.controller.AbstractController;
import com.df.renren.entity.DynamicEntity;
import com.df.renren.entity.UserAttentionEntity;
import com.df.renren.service.DynamicService;
import com.df.renren.service.FileService;
import com.df.renren.service.UserAttentionService;
import com.df.renren.utils.R;

@RestController
@RequestMapping("/app/dynamic")
public class AppDynamicController extends AbstractController{

	 @Autowired
	 private DynamicService dynamicService;
	
	 @Autowired
	 private FileService fileService;
	 
	 @Autowired
	 private UserAttentionService userAttentionService;
	
	/**
	 * 取好友动态列表
	 */
	@RequestMapping("/getDynamics")
	public R dlist(Integer page, Integer limit){
		Long uid = getAppUser().getUserId();
		List<UserAttentionEntity> uaelist = userAttentionService.findUserByuid(uid);
		if(uaelist==null||uaelist.size()==0){
			userAttentionService.addAttention(uid, uid);
		}
		List<Map<String,Object>> list = dynamicService.findById(getAppUser().getUserId(),page,limit);
		return R.ok().put("data", list);
	}
	
	/**
	 * 发表动态
	 */
	@RequestMapping(value="/createDynamic", method = RequestMethod.POST) 
	public R createDynamic(HttpServletRequest request){
		    super.req = request;
		    MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) req;
	        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
	        String realPath = req.getServletContext().getRealPath("/");
	        String webPath = realPath.replace("\\renren\\", "");
	        String urls = "";
	        for (Entry<String, MultipartFile> e : fileMap.entrySet()) {
	            urls += fileService.uploadImage(e.getValue(),webPath);
	        }
			String Content = req.getParameter("content");
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd hh:mm:ss");
			try {
				date = sdf.parse(sdf.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			DynamicEntity de = new DynamicEntity();
			de.setUserid(getAppUser().getUserId());
			de.setContent(Content);
			de.setPicture(urls);
			de.setCreateTime(date);
			dynamicService.insert(de);
			
		    return R.ok();
	}
	
	/**
	 * 删除动态
	 */
	@RequestMapping("/deleteDynamic/{id}")
	public R deleteDynamic(@PathVariable("id") Long id){
		dynamicService.deleteById(id);
		return R.ok();
	}
}
