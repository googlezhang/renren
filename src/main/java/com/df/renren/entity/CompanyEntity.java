package com.df.renren.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

@TableName(value = "company")
public class CompanyEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@TableId(type=IdType.AUTO, value="company_id")
	private Long companyId;
	private String companyName;
	private Long companyParentId;
	private String icon;
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyParentId() {
		return companyParentId;
	}
	public void setCompanyParentId(Long companyParentId) {
		this.companyParentId = companyParentId;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	

}
