package com.df.renren.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

@TableName(value = "sys_user_attention")
public class UserAttentionEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@TableId(type=IdType.AUTO,value="id")
	private Long id;
	private Long uid;
	private Long auid;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Long getAuid() {
		return auid;
	}
	public void setAuid(Long auid) {
		this.auid = auid;
	}

}
