$(function () {
    $("#jqGrid").jqGrid({
        url: '../sys/company/list',
        datatype: "json",
        colModel: [			
			{ label: '单位ID', name: 'companyId', width: 45, key: true },
			{ label: '单位名称', name: 'companyName', width: 75 },
			{ label: '上级单位ID', name: 'companyParentId', width: 90 },
			{ label: '图标', name: 'icon', width: 100 }
		],
		viewrecords: true,
        height: 400,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		
	},
	methods: {
		update: function (event) {
			var companyId = getSelectedRow();
			if(companyId == null){
				return ;
			}
			
			location.href = "company_add.html?companyId="+companyId;
		},
		del: function (event) {
			var companyId = getSelectedRows();
			if(companyId == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: "../sys/companyId/delete",
				    data: JSON.stringify(companyId),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		}
	}
});