//用户ID
var companyId = T.p("companyId");
var vm = new Vue({
	el:'#rrapp',
	data:{
		title:"新增单位",
		company:{}
	},
	created: function() {
		if(companyId != null){
			this.title = "修改单位";
			this.getInfo(companyId)
		}
		
    },
	methods: {
		getInfo: function(companyId){
			$.get("../sys/company/info/"+companyId, function(r){
				vm.company = r.company;
			});
		},
		saveOrUpdate: function (event) {
			var url = vm.company.companyId == null ? "../sys/company/save" : "../sys/company/update";
			$.ajax({
				type: "POST",
			    url: url,
			    data: JSON.stringify(vm.company),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.back();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		back: function (event) {
			history.go(-1);
		}
	}
});