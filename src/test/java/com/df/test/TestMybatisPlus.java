package com.df.test;

import org.apache.log4j.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.Serializable;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import com.baomidou.mybatisplus.plugins.Page;
import com.df.renren.entity.CompanyEntity;
import com.df.renren.entity.SysUserEntity;
import com.df.renren.service.CompanyService;
import com.df.renren.service.JedisService;
import com.df.renren.service.SysUserService;
import com.df.renren.utils.EhcacheUtil;

import redis.clients.jedis.Jedis;

public class TestMybatisPlus {

	Logger logger = Logger.getLogger("TestMybatisPlus");
	// SysUserService service = null;
	// CompanyService service = null;
//	JedisConnectionFactory jedisfactory = null;
//	Jedis jedis = null;
	
	JedisService jedisService;
	
	@Before
	public void init() {

		// ApplicationContext aCtx = new
		// FileSystemXmlApplicationContext("classpath:spring-renren.xml");
		// service = (SysUserService) aCtx.getBean("sysUserService");
		// assertNotNull(aCtx);
		// this.service = service;
		ApplicationContext aCtx = new FileSystemXmlApplicationContext("classpath:spring-renren.xml");
		jedisService = (JedisService) aCtx.getBean("jedisService");
		assertNotNull(aCtx);
	}

	@Test
	public void test() {

		// SysUserEntity user = service.queryByUserName("root");
		// System.out.println(user.getUserId() + "---->" + user.getUsername());

		// CompanyEntity entity = new CompanyEntity();
		// entity.setCompanyId(new Long(1));
		// entity.setCompanyName("2222");
		// entity.setCompanyParentId(new Long(0));
		// entity.setIcon("icon");
		// boolean res = service.updateById(entity );
		// System.out.println(res);
		// Serializable id = 1;
		// CompanyEntity c = service.selectById(id);
		// System.out.println(c.getCompanyParentId());
		// Long id = new Long(0);
		// List<CompanyEntity> list = service.findByPid(id);
		// System.out.println(list.size());

		// int page = 1;
		// int limit = 10;
		// Page<CompanyEntity> _page = new Page<CompanyEntity>(page, limit);
		// Page<CompanyEntity> pagelist = service.selectPage(_page);
		// System.out.println(pagelist.getCurrent());
		// System.out.println(pagelist.getTotal());
		// System.out.println(pagelist.getRecords().get(0).getCompanyName());
//		SysUserEntity user = new SysUserEntity();
//		user.setUserId((long) 1);
//		user.setUsername("cdftest1");
//		//String res = jedisService.set("user1", user);
//		//System.out.println(res);
//		SysUserEntity ruser = jedisService.get("user1", SysUserEntity.class);
//		System.out.println(ruser.getUsername());
		EhcacheUtil util = EhcacheUtil.getInstance();
		SysUserEntity user = new SysUserEntity();
		user.setUserId((long)1);
		user.setUsername("哈哈cdf");
		util.put("cacheTest", "uToken1", user);
		SysUserEntity obj = (SysUserEntity) util.get("cacheTest", "uToken1");
		System.out.println(obj.getUsername());
		try {
			Thread.sleep(11000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//token 10秒后失效
		SysUserEntity obj1 = (SysUserEntity) util.get("cacheTest", "uToken1");
		System.out.println(obj1.getUsername());
	}

	
}
