1 初始用户    admin-admin123
2 url     http://localhost:8080/renren/login.html
3 系统默认用的单库

4 读写分离技术利用spring的AbstractRoutingDataSource类封装，aop实现读写分离
5 主从配置-读写分离：
----5.1: 主库sql命令：GRANT REPLICATION SLAVE ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY 'cdf123';
SHOW MASTER STATUS;	  拿到file和position值	(mysqlmaster-bin.000001,332)
----5.2: 从库sql命令: STOP　SLAVE;		
CHANGE MASTER TO MASTER_HOST='127.0.0.1',MASTER_USER='root',MASTER_PASSWORD='cdf123',MASTER_LOG_FILE='mysqlmaster-bin.000001',MASTER_LOG_POS=332;(替换file和positon值)  	
----5.3: START SLAVE;	
----5.4: 查看主从配置状态，在从库sql命令：show slave status
当slave_io_running和slave_sql_running值都为yes时主从配置成功
----5.5: 修改项目中spring-renren的配置，引入spring-jdbc.xml(主从配置)，注释掉spring-jdbc-single.xml(单库配置);
----5.6: 重启项目
	
6  该系统的主从配置读写分离实现非常简单，适用场景很小，大公司的数据库集群，一般由独立的中间间做统一的管理，数据节点分配等，例如mycat	